%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.1 Ecrire un programme qui lit un fichier image en couleur et qui le sauve au format JPEG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = "Database.jpg";      % nom de l'image
im = imread(filename);          % lecture de l'image
imwrite(im,'Image.jpg');        % enregistrement de l'image

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.2 Essayer différente valeur du coefficient Q
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = "Database.jpg";          % nom de l'image
im = imread(filename);              % lecture de l'image
q = [100 80 65 30 15 10 5 1];       % liste des coefficients de qualités
compteur = 1;                       % compteur pour le subplot
figure;
for i = q
    file="image"+string(i)+".jpg";   % nom de l'image enregistrer sur le disque
    imwrite(im,file,'Quality',i) ;   % enregistrement de l'image
    ime = imread(file);              % lecture de l'image
    information = imfinfo(file)  ;   % information sur l'images
    subplot(2,4,compteur);
    imshow(ime);
    title("size ="+string(information.FileSize)+ " avec Q = "+string(i));
    compteur = compteur +1 ;         % compteur pour le subplot
end
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.3 Considerer maintenant deux images a contenus visuels differents
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = "image1.jpeg";           % nom de l'image
im = imread(filename);              % lecture de l'image
q = [100 80 65 30 15 10 5 1];       % liste des coefficients de qualites
compteur = 1   ;                    % compteur pour le subplot
figure;
for i = q
    file="image"+string(i)+".jpg";   % nom de l'image enregistrer sur le disque
    imwrite(im,file,'Quality',i);    % enregistrement de l'image
    ime = imread(file);              % lecture de l'image
    information = imfinfo(file)  ;   % information sur l'images
    subplot(2,4,compteur);
    imshow(ime);
    title("size ="+string(information.FileSize)+ " avec Q = "+string(i));
    compteur = compteur +1 ;        % compteur pour le subplot
end

filename = "image2.png";            % nom de l'image
im = imread(filename);              % lecture de l'image
q = [100 80 65 30 15 10 5 1];       % liste des coefficients de qualites
compteur = 1;                       % compteur pour le subplot
figure;
for i = q
    file="image"+string(i)+".jpg";  % nom de l'image enregistrer sur le disque
    imwrite(im,file,'Quality',i) ;  % enregistrement de l'image
    ime = imread(file);             % lecture de l'image
    information = imfinfo(file)  ;  % information sur l'images
    subplot(2,4,compteur);
    imshow(ime);
    title("size ="+string(information.FileSize)+ " avec Q = "+string(i));
    compteur = compteur +1;          % compteur pour le subplot
end
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 1.4 Prendre une image et ajouter un bruit de type Gaussien
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filename = "Database.JPG";          % nom de l'image
im = imread(filename);              % lecture de l'image
im_noise=imnoise(im,'gaussian',0.5);%ajout du brouit gaussien
figure;
subplot(1,2,1);
imshow(im);
title("Image originale");
subplot(1,2,2);
imshow(im_noise);
title("Image noise");
figure;
q = [100 80 65 30 15 10 5 1];       % liste des coefficients de qualités
compteur = 1;                       % compteur pour le subplot
for i = q
    file="image"+string(i)+".jpg" ;         % nom de l'image enregistrer sur le disque
    imwrite(im_noise,file,'Quality',i) ;    % enregistrement de l'image
    ime = imread(file);                     % lecture de l'image
    information = imfinfo(file)  ;          % information sur l'images
    subplot(2,4,compteur);
    imshow(ime);
    title("size ="+string(information.FileSize)+ " avec Q = "+string(i));
    compteur = compteur +1 ;                % compteur pour le subplot
end
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.1 Tracer la courbe de variation de Q en fonction du rapport de compression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = "Database.JPG";                  % nom de l'image
imref = imread(filename);                   % lecture de l'image
q = [100 80 65 30 15 10 5 1];               % liste des coefficients de qualités
im_initiale=imfinfo(filename);
im_initiale=im_initiale.FileSize ;          % taille de l'image originale
RC = [] ;                                   % rapport de compression
figure;
for i = q
    file="image"+string(i)+".jpg";
    imwrite(imref,file,'Quality',i);        % enregistrement de l'image
    information = imfinfo(file);
    information_t = information.FileSize;   % taille de l'image compressée
    RC=[RC im_initiale/information_t] ;     % rapport de compression = taille initiale/ taille finale
end
plot(RC,q,'LineWidth',2)
title("Facteur de Qualité en fonction du rapport de compression")
xlabel("rapport de compression")
ylabel("facteur de qualité")
grid on
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.2 Rappeler la definition de l'index de qualite objective « PSNR »
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filename = "Database.JPG";              % nom de l'image
imref = imread(filename);               % lecture de l'image
q = [100 80 65 30 15 10 5 1];           % liste des coefficients de qualités
im_initiale=imfinfo(filename);
im_initiale=im_initiale.FileSize ;      % taille de l'image originale
RC = []    ;                            % rapport de compression
peaksnr_tab=[];                         % liste de pnsr
figure;
for i = q
    file="image"+string(i)+".jpg";
    imwrite(imref,file,'Quality',i);        % enregistrement de l'image
    ime=imread(file);
    peaksnr = psnr(ime,imref);              % calcul du psnr
    peaksnr_tab=[peaksnr_tab peaksnr];
    information = imfinfo(file);
    information_t = information.FileSize;   % taille de l'image compressée
    RC=[RC im_initiale/information_t] ;     % rapport de compresion = taille initiale/ taille finale
end
plot(RC,q,RC,peaksnr_tab,'LineWidth',2)
legend('q','psnr')
title("PSNR et facteur de qualité")
xlabel("rapport de compression")
grid on
clear
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.3 Tracer la courbe de variation du PSNR en fonction du rapport de compression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filename = "Database.JPG";              % nom de l'image
imref = imread(filename);               % lecture de l'image
q = [100 80 65 30 15 10 5 1];           % liste des coefficients de qualités
im_initiale=imfinfo(filename);
im_initiale=im_initiale.FileSize ;      % taille de l'image originale
RC = []    ;                            % rapport de compression
peaksnr_tab=[];                         % liste de pnsr
figure;
for i = q
    file="image"+string(i)+".jpg";
    imwrite(imref,file,'Quality',i);        % enregistrement de l'image
    ime=imread(file);
    peaksnr = psnr(ime,imref);              % calcul du psnr
    peaksnr_tab=[peaksnr_tab peaksnr];
    information = imfinfo(file);
    information_t = information.FileSize;   % taille de l'image compressée
    RC=[RC im_initiale/information_t] ;     % rapport de compresion = taille initiale/ taille finale
end
plot(RC,peaksnr_tab,'LineWidth',2)
title("Variation du PSNR en fonction du rapport de compression")
ylabel("Variation du PSNR")
xlabel("rapport de compression")
grid on
clear
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2.4 Refaire la meme etude en considerant avec reference SSIM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = "Database.JPG";              % nom de l'image
imref = rgb2gray(imread(filename));     % en niveau de gris pour le ssim
q = [100 80 65 30 15 10 5 1];           % liste des coefficients de qualités
im_initiale=imfinfo(filename);
im_initiale=im_initiale.FileSize ;      % taille de l'image originale
RC = []    ;                            % rapport de compression
ssimval_tab=[];                         % liste de SSIM
figure;
for i = q
    file="image"+string(i)+".jpg";
    imwrite(imref,file,'Quality',i);        % enregistrement de l'image
    ime=imread(file);
    ssimval = ssim(ime,imref);              % calcul du SSIM
    ssimval_tab=[ssimval_tab ssimval];
    information = imfinfo(file);
    information_t = information.FileSize;   % taille de l'image compressée
    RC=[RC im_initiale/information_t] ;     % rapport de compresion = taille initiale/ taille finale
end
plot(RC,ssimval_tab,'LineWidth',2)
title("Variation du SSIM en fonction du rapport de compression")
xlabel("rapport de compression")
ylabel("Variation du SSIM")
grid on
clear
clc