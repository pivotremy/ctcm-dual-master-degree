% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % 2 vidéo originale et sa version compressee
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% delete video_* ;                                     
in_filename = "video_orig_mall.mp4";  
out_filename='video_';
command='ffmpeg -i '+string(in_filename)+ ' -c:v libx264 -crf 16 -preset fast -c:a aac -an '+out_filename+'.mp4';
status=system(command);
if status ~= 0
    disp("Erreur sur la commande de compression");
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2 (suite) Encoder la meme video originale e differents taux de compression
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
delete video_* ; % suppression des vidéos précedentes 
taux_crf = [1 6 12 18 24 30 36 42 50]; 
in_filename = "videoInit.mp4";  
for i = taux_crf
    videoname="video_"+string(i)+".mp4";   
    command='ffmpeg -i '+ in_filename + ' -c:v libx264 -crf '+ string(i)+ ' -preset fast -c:a aac -an '+videoname;
    status=system(command); % lancement de la compression
    if(status) ~= 0
        disp("Erreur sur la commande de compression");
        break;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 3. Pour chacune des videos codees calculer le PSNR trame par trame
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc;clear;
delete video_* ;
in_filename = "videoInit.mp4"; % video initiale
videoref = VideoReader(in_filename);
videoref.CurrentTime=0;
framesref = read(videoref,[1 Inf]);
numFrames = videoref.NumFrames;
taux_crf = [1 6 12 18 24 30 36 42 50]; 
liste_mean_psnr=[];
figure;
for i = taux_crf
    videoname="video_taux_"+string(i)+".mp4";   % nom de l'image enregistrer sur le disque
    command='ffmpeg -i '+ in_filename + ' -c:v libx264 -crf '+ string(i)+ ' -preset fast -c:a aac -an '+videoname;
    status=system(command);  % lancement de la compression
    v = VideoReader(videoname); % lecture de la videos compressee
    v.CurrentTime=0;
    vframes = read(v,[1 Inf]);  % lecture du frame
    peak_psnrs = [];
    for p = 1:numFrames
        peak_psnrs=[peak_psnrs psnr(vframes(:,:,:,p),framesref(:,:,:,p))];
    end
    liste_mean_psnr=[ liste_mean_psnr mean(peak_psnrs)];
    plot(peak_psnrs);
    hold on;
end
grid on;
title("PSNR - tracer de l'évolution de cette métrique");
ylabel("PSNR");
legend("crf=1","crf=6","crf=12","crf=18","crf=24","crf=30","crf=36","crf=42","crf=50");

figure;
grid on;
plot(taux_crf,liste_mean_psnr);
title("Evolution du PSNR en fonction de CRF");
ylabel("PSNR video");
xlabel("CRF");
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 4. Etudier l'effet du bruit d.acquisition sur les performances du codeur 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
delete video_*
clc;clear;

in_filename = "videoInit.mp4";
videoref = VideoReader(in_filename);
numFrames = videoref.NumFrames;
videoref.CurrentTime=0;
vframes = read(videoref,[1 Inf]);
for p = 1:numFrames
    vframes(:,:,:,p)=imnoise(vframes(:,:,:,p),'gaussian',0.5);%ajout du brouit gaussien
end
VideoIntermediaire="VideoTemp"
v = VideoWriter(VideoIntermediaire);
open(v);
writeVideo(v,vframes);
close(v);
in_filename = VideoIntermediaire+'.avi';
videoref = VideoReader(in_filename);
videoref.CurrentTime=0;
framesref = read(videoref,[1 Inf]);
numFrames = videoref.NumFrames;
taux_crf = [1 6 12 18 24 30 36 42 50]; 
liste_mean_psnr=[];
figure;
for i = taux_crf
    videoname="video_taux_"+string(i)+".mp4";   % nom de l'image enregistrer sur le disque
    command='ffmpeg -i '+ in_filename + ' -c:v libx264 -crf '+ string(i)+ ' -preset fast -c:a aac -an '+videoname;
    status=system(command);  % lancement de la compression
    v = VideoReader(videoname); % lecture de la videos compressee
    v.CurrentTime=0;
    vframes = read(v,[1 Inf]);  % lecture du frame
    peak_psnrs = [];
    for p = 1:numFrames
        peak_psnrs=[peak_psnrs psnr(vframes(:,:,:,p),framesref(:,:,:,p))];
    end
    liste_mean_psnr=[ liste_mean_psnr mean(peak_psnrs)];
    plot(peak_psnrs);
    hold on;
end
grid on;
title("PSNR - tracer de l'évolution de cette métrique avec un bruit gaussien");
ylabel("PSNR");
legend("crf=1","crf=6","crf=12","crf=18","crf=24","crf=30","crf=36","crf=42","crf=50");

figure;
plot(taux_crf,liste_mean_psnr);
grid on;
title("Evolution du PSNR en fonction de CRF avec un bruit gaussien");
ylabel("PSNR video");
xlabel("CRF");

% %%%%%%%%%%%%%%%%%
% %   SSIM
% %%%%%%%%%%%%%%%%%
delete video_*;
delete videoT*;
clc;clear;
in_filename = "videoInit.mp4";
videoref = VideoReader(in_filename);
numFrames = videoref.NumFrames;
videoref.CurrentTime=0;
vframes = read(videoref,[1 Inf]);
for p = 1:numFrames
    vframes(:,:,:,p)=imnoise(vframes(:,:,:,p),'gaussian',0.5);%ajout du brouit gaussien
end
VideoIntermediaire="VideoTemp"
v = VideoWriter(VideoIntermediaire);
open(v);
writeVideo(v,vframes);
close(v);
in_filename = VideoIntermediaire+'.avi';
videoref = VideoReader(in_filename);
videoref.CurrentTime=0;
framesref = read(videoref,[1 Inf]);
numFrames = videoref.NumFrames;
taux_crf = [1 6 12 18 24 30 36 42 50]; 
liste_mean_ssim=[];
figure;
for i = taux_crf
    videoname="video_taux_"+string(i)+".mp4";   % nom de l'image enregistrer sur le disque
    command='ffmpeg -i '+ in_filename + ' -c:v libx264 -crf '+ string(i)+ ' -preset fast -c:a aac -an '+videoname;
    status=system(command);  % lancement de la compression
    v = VideoReader(videoname); % lecture de la videos compressee
    v.CurrentTime=0;
    vframes = read(v,[1 Inf]);  % lecture du frame
    ssimval_tab = [];
    for p = 1:numFrames
        ssimval_tab=[ssimval_tab psnr(rgb2gray(vframes(:,:,:,p)),rgb2gray(framesref(:,:,:,p)))];
    end
    liste_mean_ssim=[ liste_mean_ssim mean(ssimval_tab)];
    plot(ssimval_tab);
    hold on;
end
grid on;
title("SSIM - tracer de l'évolution de cette métrique avec un bruit gaussien");
ylabel("SSIM");
legend("crf=1","crf=6","crf=12","crf=18","crf=24","crf=30","crf=36","crf=42","crf=50");

figure;
plot(taux_crf,liste_mean_ssim);
grid on;
title("Evolution du SSIM en fonction de CRF avec un bruit gaussien");
ylabel("SSIM video");
xlabel("CRF");

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   2.5 Cette dernière expérience est laissée à l2initiative de chaque binôme
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc;clear;
delete videoH* ;
in_filename = "videoInit.mp4"; % video initiale
taux_crf = [1 6 12 18 24 30 36 42 50]; 
for i = taux_crf
    videoname="videoH265_taux_"+string(i)+".mp4";   % nom de l'image enregistrer sur le disque
    command='ffmpeg -i '+ in_filename + ' -c:v libx265 -crf '+ string(i)+ ' -preset fast -c:a aac -an '+videoname;
    status=system(command);  % lancement de la compression

    videoname="videoH264_taux_"+string(i)+".mp4";   % nom de l'image enregistrer sur le disque
    command='ffmpeg -i '+ in_filename + ' -c:v libx264 -crf '+ string(i)+ ' -preset fast -c:a aac -an '+videoname;
    status=system(command);  % lancement de la compression
end